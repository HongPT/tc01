import { Selector } from "testcafe";

fixture`test login`.page`https://tiki.vn/`;

const search = Selector("input").withAttribute(
  "data-view-id",
  "main_search_form_input"
);

const searchText = Selector("input[data-view-id='main_search_form_input']");

test("My first test", async (t) => {
  // Test code

  await t

    // .typeText(search, "cơm")

    .typeText(searchText, "cơm")

    .wait(5000);
});
