import { Selector } from "testcafe";

fixture`Example`.page`https://tiki.vn/`;
const scrolItem = Selector(
  "body > div:nth-child(2) > div:nth-child(1) > main:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(5) > a:nth-child(1) > span:nth-child(1) > div:nth-child(1) > div:nth-child(1) > picture:nth-child(3) > img:nth-child(2)"
);

test("Type Text test", async (t) => {
  await t
    .click(
      "div[class='style__StyledCategoryTabBarWrapper-sc-x9fskq-0 NNpEP'] a:nth-child(2) div:nth-child(1)"
    )
    .wait(5000)
    .scrollIntoView(scrolItem)
    .wait(5000);
});
