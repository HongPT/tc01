import { ClientFunction } from "testcafe";
import LoginPage from "../Page/LoginPage";

const url = "https://tiki.vn/";
const getUrl = ClientFunction(() => window.location.href);
fixture("tiki").page(url);

test("Loading Tiki", async (t) => {
  await t
    .expect(getUrl())
    .contains(url)
    .expect(LoginPage.btnSearch.exists)
    .ok();
});

test("search ", async (t) => {
  LoginPage.setInput("chuot");
  LoginPage.clickBtn();
});
