import { Selector } from "testcafe";

fixture`Example`.page`https://devexpress.github.io/testcafe/example/`;

test("Drag test", async (t) => {
  const triedCheckbox = Selector("label").withAttribute(
    "for",
    "tried-test-cafe"
  );

  await t

    .click(triedCheckbox)

    .drag("#slider", 360, 0, { offsetX: 10, offsetY: 10 })

    //.takeScreenshot({

    //path:     'thank-you-page.png',

    //fullPage: true

    //})

    .expect(await triedCheckbox.innerText)
    .eql("I have tried TestCafe", "Tex matched")

    .wait(2000);
});
