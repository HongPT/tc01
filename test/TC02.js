import { Selector } from "testcafe";

fixture`Example`.page`https://tiki.vn/`;

const clickButton = Selector("button[data-view-id='main_search_form_button']");

const PName = Selector(
  "img[alt='Quần sơ sinh Ozaha, quần dài sơ sinh, quần chục sơ sinh, quần đóng bỉm cho bé']"
);
const PName2 = Selector(
  "div[class='thumbnail'] img[alt='Quần sơ sinh Ozaha, quần dài sơ sinh, quần chục sơ sinh, quần đóng bỉm cho bé']"
);

test("Type Text test", async (t) => {
  await t
    .typeText(".FormSearch__Input-sc-1fwg3wo-2 gBxvSE", "Quan Ao")
    .wait(5000)
    .click(clickButton)
    .wait(5000)

    .click(PName2)
    .wait(5000);
});
