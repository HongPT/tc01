import { Selector } from "testcafe";

fixture`Example`.page`https://demoqa.com/elements`;
test("Drag test", async (t) => {
  await t
    .expect(Selector(".header-wrapper").count)
    .eql(6, "Accordion Items Count", { timeout: 5000 });
});
