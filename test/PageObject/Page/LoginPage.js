import { Selector, t } from "testcafe";

class LoginPage {
  constructor() {
    this.Inputtext = Selector("input[data-view-id='main_search_form_input']");
    this.btnSearch = Selector("button[data-view-id='main_search_form_button']");
  }

  async setInput(input) {
    await t.typeText(this.Inputtext, input);
  }
  async clickBtn() {
    await t.click(this.btnSearch);
  }
}
export default new LoginPage();
